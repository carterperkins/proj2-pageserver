from flask import Flask, render_template

app = Flask(__name__)

# Symbols that raise a 403 error if found in the url
FORBIDDEN_SYMBOLS = ['//', '..', '~']

# Main page
@app.route('/')
def index():
    '''
    Called on root directory of the server (i.e. localhost:5000). Renders basic test
    message.
    
    Returns:
        Basic string message
    '''
    return "Main Index\n"

# Use Flask path so user can attempt to work through multiple directories.
@app.route('/<path:arg>')
def main(arg):
    '''
    Called whenever a path is appended to root directory of the server (i.e. localhost:5000/trivia.html) 
    and renders valid html request or appropriate error message.

    Args:
        arg: URL path
    Returns:
        HTML file if it exists, otherwise error message.
    '''
    # If the URL contains a forbidden symbol raise a 403 error
    # Check for forbidden symbols before trying to render a template in case
    # the template does not exist and save optimizie the code.
    for SYMBOLS in FORBIDDEN_SYMBOLS:
        if SYMBOLS in arg:
            return forbidden_error(Exception)
    # Make sure file is a valid file to read (.html), only want to render html files
    if '.html' not in arg:
        return forbidden_error(Exception)
    # Try to load html file from path, if there is an exception raise a 404 error
    try:
        return render_template(arg), arg
    except Exception as e:
        return not_found_error(e)


# Flask error handlers for 404 and 403 errors
@app.errorhandler(404)
def not_found_error(e):
    return render_template('404.html'), 404
    
@app.errorhandler(403)
def forbidden_error(e):
    return render_template('403.html'), 403

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)
